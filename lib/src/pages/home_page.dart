import 'package:flutter/material.dart';


class HomePage extends StatelessWidget {
  final conteo = 10;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Material App Bar'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Numero de Clicks', style: TextStyle(fontSize: 25),),
              Text('$conteo', style: TextStyle(fontSize: 25),),
            ],
          ),
        ),

        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () => {
            print('Hola bender!!')
            //conteo += conteo
            },
          ),

      ),
    );
  }
}