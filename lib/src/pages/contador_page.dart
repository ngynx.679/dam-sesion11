import 'package:flutter/material.dart';

class MyAppFull extends StatefulWidget {
  @override
  _MyAppFullState createState() => _MyAppFullState();
}

class _MyAppFullState extends State<MyAppFull> {
  int conteo = 0;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Material App Bar'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Numero de Clicks', style: TextStyle(fontSize: 25),),
              Text('$conteo', style: TextStyle(fontSize: 25),),
            ],
          ),
        ),
        floatingActionButton: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            FloatingActionButton(
              child: Icon(Icons.remove),
              onPressed: () {
                if (conteo > 0) {
                  setState(() {
                    conteo--;
                  });
                }
              },
              backgroundColor: Colors.red,
            ),
            FloatingActionButton(
              child: Icon(Icons.refresh,),
              onPressed: () {
                setState(() {
                  conteo = 0;
                });
              },
              backgroundColor: Colors.red,

            ),
            FloatingActionButton(
              child: Icon(Icons.add),
              onPressed: () {
                setState(() {
                  conteo++;
                });
              },
              backgroundColor: Colors.red,
            )
          ],
        ),
      ),
    );
  }
}